package com.surecloud.javatechnicalinterview;

import com.surecloud.javatechnicalinterview.dto.ResultDto;
import com.surecloud.javatechnicalinterview.model.Result;
import com.surecloud.javatechnicalinterview.service.ResultService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.Calendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = JavaTechnicalInterviewApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTests {

    private String url;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeEach
    public void setup() {
        url = "http://localhost:" + port + "/result/";
    }

    @Test
    public void testGetAllResults() {
        List testResult = this.restTemplate.getForObject( url, List.class);
        assertTrue(testResult.size() > 1);
    }

    @Test
    public void testGetResultById() {
        String id = "ff853cf3-6c94-4a79-b9f9-459bc2a6f980";
        ResultDto testResult = this.restTemplate.getForObject( url+ id, ResultDto.class);
        assertEquals(testResult.getId(), id);
    }

    @Test
    public void testGetResultByIdWhenIdDoesntExistShouldNotFindItem() {
        String id = "1234";
        ResultDto testResult = this.restTemplate.getForObject( url+ id, ResultDto.class);
        assertNull(testResult.getId());
    }

    @Test
    public void testCreateNewResultShouldExistAfterward() {
        ResultDto resultDto = ResultDto.builder()
                .name("name")
                .score(39)
                .dateTaken(Calendar.getInstance())
                .build();

        ResultDto testResult = this.restTemplate.postForObject(url, resultDto, ResultDto.class);

        assertNotNull(testResult.getId());
        assertEquals(testResult.getName(), resultDto.getName());
        assertEquals(testResult.getScore(), resultDto.getScore());
        assertEquals(testResult.getDateTaken().getTimeInMillis(), resultDto.getDateTaken().getTimeInMillis());

        String resultId = testResult.getId();
        ResultDto testResult2 = this.restTemplate.getForObject( url+ resultId, ResultDto.class);
        assertEquals(testResult2.getId(), resultId);
        assertEquals(testResult2.getName(), resultDto.getName());
        assertEquals(testResult2.getScore(), resultDto.getScore());
    }
}
