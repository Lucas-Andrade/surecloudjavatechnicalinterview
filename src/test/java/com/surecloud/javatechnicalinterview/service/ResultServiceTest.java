package com.surecloud.javatechnicalinterview.service;

import com.surecloud.javatechnicalinterview.dto.ResultDto;
import com.surecloud.javatechnicalinterview.exception.ResultNotFound;
import com.surecloud.javatechnicalinterview.model.Result;
import com.surecloud.javatechnicalinterview.repository.ResultRepository;
import com.surecloud.javatechnicalinterview.service.ResultService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ResultServiceTest {
    @Mock
    ResultRepository resultRepositoryMock;

    @InjectMocks
    ResultService testObj;

    @BeforeEach
    public void setup() {
        testObj = new ResultService(resultRepositoryMock);
    }

    @Test
    void gettingByIdShouldReturnTheResult() {
        //given
        Result result = Result.builder()
                .id("id")
                .name("name")
                .score(39)
                .dateTaken(Calendar.getInstance())
                .build();
        when(resultRepositoryMock.findById(result.getId())).thenReturn(Optional.of(result));

        //when
        ResultDto testResult = testObj.getById(result.getId());

        //then
        assertEquals(result.getId(), testResult.getId());
        assertEquals(result.getScore(), testResult.getScore());
        assertEquals(result.getDateTaken(), testResult.getDateTaken());
        assertEquals(result.getName(), testResult.getName());
    }

    @Test
    void gettingByIdSWhenTheIdDoesntExistShouldReturnTheResult() {
        String resultId = "some id";
        when(resultRepositoryMock.findById(resultId)).thenReturn(Optional.empty());

        assertThrows(ResultNotFound.class, () -> testObj.getById(resultId));
    }

    @Test
    void gettingAllShouldReturnAListOfResults() {
        Result result1 = Result.builder()
                .id("id1")
                .name("name1")
                .score(1)
                .dateTaken(Calendar.getInstance())
                .build();
        Result result2 = Result.builder()
                .id("id2")
                .name("name2")
                .score(2)
                .dateTaken(Calendar.getInstance())
                .build();
        List<Result> resultsList = Arrays.asList(result1,result2);

        when(resultRepositoryMock.findAll()).thenReturn(resultsList);

        List<ResultDto> testResult = testObj.getAll();

        assertEquals(2, testResult.size());
        assertEquals(result1.getId(), testResult.get(0).getId());
        assertEquals(result2.getId(), testResult.get(1).getId());
    }

    @Test
    void gettingAllWhenThereArentAnyShouldReturnAnEmptyList() {
        when(resultRepositoryMock.findAll()).thenReturn(new ArrayList<>());

        List<ResultDto> testResult = testObj.getAll();

        assertEquals(0, testResult.size());
    }

    @Test
    void creatingANewResultShouldSaveTheResult() {
        ResultDto resultDto = ResultDto.builder()
                .name("name")
                .score(39)
                .dateTaken(Calendar.getInstance())
                .build();

        when(resultRepositoryMock.save(any())).thenReturn(Result.builder()
                .id("new id")
                .name(resultDto.getName())
                .score(resultDto.getScore())
                .dateTaken(resultDto.getDateTaken())
                .build());

        ResultDto testResult = testObj.create(resultDto);

        verify(resultRepositoryMock, times(1)).save(
                argThat(r -> {
                    assertThat(r).isNotNull();
                    assertThat(r.getName()).isEqualTo(resultDto.getName());
                    assertThat(r.getScore()).isEqualTo(resultDto.getScore());
                    assertThat(r.getDateTaken()).isEqualTo(resultDto.getDateTaken());
                    return true;
                })
        );
        assertEquals(resultDto.getScore(), testResult.getScore());
        assertEquals(resultDto.getDateTaken(), testResult.getDateTaken());
        assertEquals(resultDto.getName(), testResult.getName());
    }
}
