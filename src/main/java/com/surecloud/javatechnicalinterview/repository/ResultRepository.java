package com.surecloud.javatechnicalinterview.repository;

import com.surecloud.javatechnicalinterview.model.Result;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ResultRepository extends CrudRepository<Result, String> {
}
