package com.surecloud.javatechnicalinterview.service;

import com.surecloud.javatechnicalinterview.dto.ResultDto;
import com.surecloud.javatechnicalinterview.exception.ResultNotFound;
import com.surecloud.javatechnicalinterview.model.Result;
import com.surecloud.javatechnicalinterview.repository.ResultRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@RequiredArgsConstructor
public class ResultService {

    protected final Log logger = LogFactory.getLog(this.getClass());

    private final ResultRepository resultRepository;

    public ResultDto getById(String id) {
        Optional<Result> resultOptional = resultRepository.findById(id);
        if(resultOptional.isPresent()){
            logger.debug(String.format("Getting Result with id %s", id));
            return ResultDto.getInstance(resultOptional.get());
        } else {
            logger.warn("Tried to get Result by an ID that does not exist.");
            throw new ResultNotFound();
        }
    }

    public List<ResultDto> getAll() {
        Iterable<Result> allResults = resultRepository.findAll();
        logger.debug("Getting all Results.");
        return StreamSupport.stream(allResults.spliterator(), false)
                .map(ResultDto::getInstance)
                .collect(Collectors.toList());
    }

    public ResultDto create(ResultDto toCreate) {
        Result result = Result.builder()
                .name(toCreate.getName())
                .dateTaken(toCreate.getDateTaken())
                .score(toCreate.getScore())
                .build();

        Result savedResult = resultRepository.save(result);

        logger.debug(String.format("Saved a new Result with id %s", savedResult.getId()));
        return ResultDto.getInstance(savedResult);
    }

}
