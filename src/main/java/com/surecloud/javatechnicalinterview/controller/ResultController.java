package com.surecloud.javatechnicalinterview.controller;

import com.surecloud.javatechnicalinterview.dto.ResultDto;
import com.surecloud.javatechnicalinterview.service.ResultService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/result")
@RequiredArgsConstructor
public class ResultController {

    private final ResultService resultService;

    @GetMapping(path = "/{id}")
    public ResultDto getById(@PathVariable String id) {
        return resultService.getById(id);
    }

    @GetMapping
    public List<ResultDto> getAll() {
        return resultService.getAll();
    }

    @PostMapping
    public ResultDto create(@RequestBody ResultDto resultDto) {
        return resultService.create(resultDto);
    }

}
