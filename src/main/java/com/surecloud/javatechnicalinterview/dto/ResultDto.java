package com.surecloud.javatechnicalinterview.dto;

import com.surecloud.javatechnicalinterview.model.Result;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Builder
@Data
public class ResultDto {

    private String id;
    @NotNull
    private String name;
    @NotNull
    private int score;
    @NotNull
    private Calendar dateTaken;

    public static ResultDto getInstance(Result result) {
        return ResultDto.builder()
                .id(result.getId())
                .name(result.getName())
                .score(result.getScore())
                .dateTaken(result.getDateTaken())
                .build();
    }
}
