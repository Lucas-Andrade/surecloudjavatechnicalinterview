package com.surecloud.javatechnicalinterview.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Result")
@Table(name = "result")
public class Result {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;

    private String name;

    private int score;

    @Temporal(TemporalType.DATE)
    private Calendar dateTaken;

}
