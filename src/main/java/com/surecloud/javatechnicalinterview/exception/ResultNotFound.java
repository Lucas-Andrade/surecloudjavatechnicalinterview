package com.surecloud.javatechnicalinterview.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Result with the specified ID was not found.")
public class ResultNotFound extends RuntimeException {

}
